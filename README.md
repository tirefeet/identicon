# Identicon

The program generates an [identicon](https://en.wikipedia.org/wiki/Identicon) from a string text using MD5 hashing.

## Topics Covered

- Hashing strings
- Applying hashes to strings
- Structs
- Pattern matching with structs
- Extracting elements from structs
- Updating structs
- List properties and tuple properties
- Accepting properties from structs with pattern matching
- List iteration
- Chunk operation
- Joining with lists
- Mapping existing lists
- Function calls with regard to arguments
- Nesting lists with `flatten()`
- Getting the indice information (Elixir by default does not store this)
- `remainder()` function to extract elements
- Erlang's [egd](https://www.erlang.org/docs/18/man/egd.html) module
- Creating blank canvas
- Anonymous functions

## How to run 

Make sure Elixir is installed on your system. You can do this from command line with:

`elixir -v`

Go to project directory and install dependencies:

`mix deps.get`

Run the project from interactive shell:

`iex -S mix`

To generate an image type the following:

`Identicon.main("<string of your choice>")`
